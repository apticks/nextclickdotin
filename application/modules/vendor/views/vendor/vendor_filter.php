 <style>
.page-item>a {
	position: relative;
	display: block;
	padding: .5rem .75rem;
	margin-left: -1px;
	line-height: 1.25;
	color: #007bff;
	background-color: #fff;
	border: 1px solid #dee2e6;
}

a {
	color: #007bff;
	text-decoration: none;
	background-color: transparent;
}

.pagination>li.active>a {
	background-color: orange !important;
}

.dataTables_filter {
	display: none;
}
.or{
    text-align: center;
}
</style>
<!--Add Category And its list-->
<!--Add Category And its list-->
<div class="row h-100 justify-content-center align-items-center">
	<div class="col-12">
    		<div class="card-header">
    			<h4 class="ven">Vendors Filter</h4>
        		 <form class="" novalidate="" action="<?php echo base_url('vendors_filter/0');?>" method="post" enctype="multipart/form-data">
        		 	<div class="row">
        				<div class="form-group col-3">
        					<label for="q">Name</label>
    						<input type="text" name="q" id="q" placeholder="Name" value="<?php echo $q;?>" class="form-control">
    					</div>
    					<div class="form-group col-2">
    						<label for="exe">Unique Id</label>
    						<input type="text" id="exe" name="exe" placeholder="Unique Id" value="<?php echo $exe;?>" class="form-control">
    					</div>
    					<div class="form-group col-3">
    						<label for="mobile">Mobile</label>
    						<input type="text" id="mobile" name="mobile" placeholder="Mobile" value="<?php echo $mobile;?>" class="form-control">
    					</div>
    					<div class="form-group col-2">
                            <label for="status">Status</label>
                            <select calss="form-control" name="status" class="form-control">
                            	<option value="1" <?php echo ($status == 1)? "selected" : ""?>>Approved</option>
                            	<option value="2" <?php echo ($status == 2)? "selected" : ""?>>Disapproved</option>
                            	<option value="3" <?php echo ($status == 3)? "selected" : ""?>>Deleted</option>
                            </select>
                        </div>
                        <div class="form-group col-2">
    						<label for="noofrows">rows</label>
    						<input type="text" id="noofrows" name="noofrows" placeholder="rows" value="<?php echo $noofrows;?>" class="form-control">
    					</div>
    					<button type="submit" name="submit" id="upload" value="Apply" class="btn btn-primary mt-27 ">Search</button>
					</div>
					
        		</form>
        		<form class="needs-validation h-100 justify-content-center align-items-center" novalidate="" action="<?php echo base_url('vendors_filter/0');?>" method="post" enctype="multipart/form-data">
    				<input type="hidden" name="q" placeholder="Search" value="" class="form-control">
    				<select calss="form-control" name="status" style="display: none" class="form-control">
                            	<option value="1" >Approved</option>
                    </select>
                    <input type="hidden" id="noofrows" name="noofrows" placeholder="rows" value="10" class="form-control">
    				<button type="submit" name="submit" id="upload" value="Apply" class="btn btn-danger mt-3">Clear</button>
    			</form>
			</div>
		</div>
	</div> 
			
		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4 class="ven">List of Vendors</h4>
					<a class="btn btn-outline-dark btn-lg col-2 pull-right"
				href="<?php echo base_url('vendor_excel_import')?>"><i
				class="fa fa-plus" aria-hidden="true"></i> Add Vendor</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover"
						id="tableExportNoPagination" style="width: 100%;">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Executive Id</th>
								<th>Identity</th>
								<th>Address</th>
								<th>Constituency</th>
								<th>Contact</th>
								<th>Category</th>
								<th>Timings</th>
        									<?php  //if( $this->ion_auth_acl->has_permission('vendor_approval')):?>
        										<th>Approve</th>
        									<?php //endif;?>
        									<th>Actions</th>
							</tr>
						</thead>
						<tbody>
        							<?php if(!empty($vendors)):?>
            							<?php $sno = 1; foreach ($vendors as $vendor):?>
            								<tr>
								<td><?php echo $sno++;?></td>
								<td><?php foreach ($executive as $ex): if($vendor['executive_id'] == $ex['id']):?>
            									<?php echo $ex['unique_id'];?>
            									<?php endif;endforeach;?></td>
								<td><?php echo $vendor['name']."<br/> ( <b>".$vendor['unique_id']." </b>)";?></td>
								<td><?php echo $vendor['location_address'];?></td>
								<td><?php foreach ($constituency as $con): if($vendor['constituency_id'] == $con['id']):?>
            									<?php echo $con['name'];?>
            									<?php endif;endforeach;?></td>
								<td><?php echo (array_search($vendor['id'], array_column($contacts, 'list_id')) !== FALSE)? $contacts[array_search($vendor['id'], array_column($contacts, 'list_id'))]['number']: ""."<br />".$vendor['email'];?></td>
								<td><?php foreach ($categories as $category): if($vendor['category_id'] == $category['id']):?>
            									<?php echo $category['name'];?>
            									<?php endif;endforeach;?></td>
								<td><?php echo date('M d, Y (H:i)', strtotime($vendor['created_at']));?></td>
								<td><input type="checkbox" class="approve_toggle"
									vendor_id="<?php echo $vendor['id'];?>"
									user_id="<?php echo $this->session->userdata('user_id');?>"
									<?php echo ($vendor['status'] == 1) ? 'checked':'' ;?>
									data-toggle="toggle" data-style="ios" data-on="Approved"
									data-off="Dispprove" data-onstyle="success"
									data-offstyle="danger"></td>
								<td>
									<!-- <a href="#" class=" mr-2  " type="category" > <i class="fas fa-pencil-alt"></i>
            									</a> --> <a href="#" class="mr-2  text-danger "
									onClick="delete_record(<?php echo $vendor['id'];?>, 'vendors')">
										<i class="far fa-trash-alt"></i>
								</a> <a
									href="<?php echo base_url();?>vendor_profile/edit?id=<?php echo $vendor['id']; ?>"
									class="mr-2  "> <i class="fas fa-pencil-alt"></i>
								</a> <!--     									<a href="<?php //base_url('vendors/vendor?vendor_id=').$vendor['id'];?>" target="_blank" class=" mr-2  " type="category" > <i class="fas fa-eye"></i> -->
									</a> <a
									href="<?=base_url('vendor_payments/r?vendor_id=').$vendor['id']."&id=".$vendor['vendor_user_id'];?>"
									target="_blank" class=" mr-2  " type="category"> <i
										class="fa fa-book"></i>
								</a>
								</td>

							</tr>
            							<?php endforeach;?>
        							<?php else :?>
        							<tr>
								<th colspan='9'><h3>
										<center>No Vendor</center>
									</h3></th>
							</tr>
        							<?php endif;?>
        							</tbody>
					</table>
					</div>
					<!-- Paginate -->
    				<div class="row  justify-content-center">
    					<div class=" col-12" style='margin-top: 10px;'>
                           <?= $pagination; ?>
                        </div>
    				</div>
				</div>
			</div>


		</div>

	</div>
</div>
