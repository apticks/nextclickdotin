<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;

class Vendor extends MY_REST_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->model('vendor_bank_details_model');
        $this->load->model('vendor_list_model');
        $this->load->model('setting_model');
        $this->load->model('contact_model');
        $this->load->model('social_model');
        $this->load->model('sub_category_model');
        $this->load->model('permission_model');
        $this->load->model('amenity_model');
        $this->load->model('vendor_amenity_model');
        $this->load->model('vendor_service_model');
        $this->load->model('vendor_sub_category_model');
        $this->load->model('vendor_brand_model');
        $this->load->model('vendor_banner_model');
        $this->load->model('wallet_transaction_model');
        $this->load->model('group_model');
        $this->load->model('location_model');
        $this->load->model('user_model');
        $this->load->model('constituency_model');
        $this->load->model('details_by_vendor_model');
        $this->load->model('package_model');
        $this->load->model('vendor_package_model');
        $this->load->model('vendor_settings_model');
        $this->load->model('food_settings_model');
        $this->load->model('vendor_leads_model');
    }

    /**
     * To manage profile 
     *
     * @author Mehar
     *        
     * @param string $type
     */
    public function profile_post($type = 'r', $u_type = 'bank_details')
    {
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        $this->vendor_list_model->user_id = $token_data->id;
        $vendor = $this->vendor_list_model->where('vendor_user_id', $token_data->id)->get();
        if ($type == 'r') {
            $this->data['vendor_details'] = $this->vendor_list_model->with_location('fields: id, address, latitude, longitude')
                ->with_category('fields: id, name')
                ->with_constituency('fields: id, name, state_id, district_id')
                ->with_contacts('fields: id, std_code, number, type')
                ->with_links('fields: id, url, type')
                ->with_amenities('fields: id, name')
                ->with_services('fields: id, name,desc,languages')
                ->with_brands('fields: id, name')
                ->with_holidays('fields: id')
                ->where('vendor_user_id', $token_data->id)
                ->get();
            if($this->data['vendor_details']['services']){foreach ($this->data['vendor_details']['services'] as $k => $v){
               $this->data['vendor_details']['services'][$k]['image'] = base_url() . "uploads/service_image/service_" . $v['id'] . ".jpg".'?'.time();
            }}
            $this->data['vendor_details']['bank_details'] = $this->vendor_bank_details_model->fields('id,bank_name,bank_branch,ifsc,ac_holder_name,ac_number')
                ->where('list_id', $this->data['vendor_details']['id'])
                ->get();
            $vendor_banners = $this->vendor_banner_model->where('list_id', $vendor['id'])->get_all();
            $this->data['vendor_details']['cover'] = base_url() . "uploads/list_cover_image/list_cover_" . $vendor['id'] . ".jpg";
            $this->data['vendor_details']['banners'] = [];
            if ($vendor_banners) {
                foreach ($vendor_banners as $key => $banner) {
                    $this->data['vendor_details']['banners'][$key]['id'] = $banner['id'];
                    $this->data['vendor_details']['banners'][$key]['image'] = base_url() . "uploads/list_banner_image/list_banner_" . $banner['id'] . ".jpg";
                }
            }
                $this->set_response_simple(($this->data['vendor_details'] == FALSE) ? FALSE : $this->data['vendor_details'], 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
        } elseif ($type == 'u') {
            if ($u_type == 'bank_details') {
                $this->form_validation->set_rules($this->vendor_bank_details_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    $this->set_response_simple(validation_errors(), "Vallidation errors", REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
                } else {
                    $this->vendor_bank_details_model->user_id = $token_data->id;
                    $r = $this->vendor_bank_details_model->fields('id')->where(['list_id' => $vendor['id'], 'created_user_id' => $token_data->id])->get();
                    if (! empty($r)) {
                        $id = $this->vendor_bank_details_model->update([
                            'list_id' => $vendor['id'], 
                            'bank_name' => $this->input->post('bank_name'),
                            'bank_branch' => $this->input->post('bank_branch'),
                            'ifsc' => $this->input->post('ifsc'),
                            'ac_holder_name' => $this->input->post('ac_holder_name'),
                            'ac_number' => $this->input->post('ac_number')
                        ],'list_id');
                        $this->set_response_simple(($id == FALSE) ? FALSE : $id, 'Updated..!', REST_Controller::HTTP_ACCEPTED, TRUE);
                    } else {
                        $id = $this->vendor_bank_details_model->insert([
                            'bank_name' => $this->input->post('bank_name'),
                            'bank_branch' => $this->input->post('bank_branch'),
                            'ifsc' => $this->input->post('ifsc'),
                            'ac_holder_name' => $this->input->post('ac_holder_name'),
                            'ac_number' => $this->input->post('ac_number'),
                            'list_id' => $vendor['id']
                        ]);
                        $this->set_response_simple(($id == FALSE) ? FALSE : $id, 'Inserted..!', REST_Controller::HTTP_CREATED, TRUE);
                    }
                }
            }
        } elseif ($type == 'profile') {
            $this->form_validation->set_rules($this->vendor_list_model->rules['profile']);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), "Vallidation errors", REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $this->vendor_list_model->update([
                    'id' => $vendor['id'],
                    'name' => $this->input->post('name'),
                    'address' => $this->input->post('address'),
                    'email' => $this->input->post('email'),
                    'landmark' => $this->input->post('landmark'),
                    'desc' => $this->input->post('desc'),
                    'availability' => $this->input->post('availability')
                ], 'id');
                
                $is_location_exist = $this->location_model->where([
                    'latitude' => $this->input->post('latitude'),
                    'longitude' => $this->input->post('longitude')
                ])->get();
                
                if (empty($is_location_exist)) {
                    $location_id = $this->location_model->insert([
                        'address' => $this->input->post('location_name'),
                        'latitude' => $this->input->post('latitude'),
                        'longitude' => $this->input->post('longitude')
                    ]);
                } else {
                    $location_id = $is_location_exist['id'];
                }
                
                $this->vendor_list_model->update([
                    'id' => $vendor['id'],
                    'location_id' => $location_id
                ], 'id');

                if($this->contact_model->where(['list_id' => $vendor['id'], 'type' => 1])->get() != FALSE)
                    $this->contact_model->update(['std_code' => $this->input->post('mobile')['code'], 'number' => $this->input->post('mobile')['number']], ['list_id' => $vendor['id'], 'type' => 1]);
                else
                    $this->contact_model->insert(['list_id' => $vendor['id'], 'std_code' => $this->input->post('mobile')['code'], 'number' => $this->input->post('mobile')['number'], 'type' => 1]);
                        
                if($this->contact_model->where(['list_id' => $vendor['id'], 'type' => 2])->get() != FALSE)
                    $this->contact_model->update(['std_code' => $this->input->post('landline')['code'], 'number' => $this->input->post('landline')['number']], ['list_id' => $vendor['id'], 'type' => 2]);
                else
                    $this->contact_model->insert(['list_id' => $vendor['id'], 'std_code' => $this->input->post('landline')['code'], 'number' => $this->input->post('landline')['number'], 'type' => 2]);
                                
                if($this->contact_model->where(['list_id' => $vendor['id'], 'type' => 3])->get() != FALSE)
                    $this->contact_model->update(['std_code' => $this->input->post('whatsapp')['code'], 'number' => $this->input->post('whatsapp')['number']], ['list_id' => $vendor['id'], 'type' => 3]);
                else
                    $this->contact_model->insert(['list_id' => $vendor['id'], 'std_code' => $this->input->post('whatsapp')['code'], 'number' => $this->input->post('whatsapp')['number'], 'type' => 3]);
                                        
                if($this->contact_model->where(['list_id' => $vendor['id'], 'type' => 4])->get() != FALSE)
                    $this->contact_model->update(['std_code' => $this->input->post('helpline')['code'], 'number' => $this->input->post('helpline')['number']], ['list_id' => $vendor['id'], 'type' => 4]);
                else
                    $this->contact_model->insert(['list_id' => $vendor['id'], 'std_code' => $this->input->post('helpline')['code'], 'number' => $this->input->post('helpline')['number'], 'type' => 4]);

               $this->set_response_simple(NULL, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
            }
        }elseif ($type == 'social') {
            $this->form_validation->set_rules($this->vendor_list_model->rules['social']);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), "Vallidation errors", REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
            if($this->social_model->where(['list_id' => $vendor['id'], 'type' => 1])->get() != FALSE)
                $this->social_model->update(['url' => $this->input->post('facebook')], ['list_id' => $vendor['id'], 'type' => 1]);
            else
                $this->social_model->insert(['list_id' => $vendor['id'], 'url' => $this->input->post('facebook'), 'type' => 1]);
                        
            if($this->social_model->where(['list_id' => $vendor['id'], 'type' => 2])->get() != FALSE)
                $this->social_model->update(['url' => $this->input->post('twitter')], ['list_id' => $vendor['id'], 'type' => 2]);
            else
                $this->social_model->insert(['list_id' => $vendor['id'], 'url' => $this->input->post('twitter'), 'type' => 2]);
                                
            if($this->social_model->where(['list_id' => $vendor['id'], 'type' => 3])->get() != FALSE)
                $this->social_model->update(['url' => $this->input->post('instagram')], ['list_id' => $vendor['id'], 'type' => 3]);
            else
                $this->social_model->insert(['list_id' => $vendor['id'], 'url' => $this->input->post('instagram'), 'type' => 3]);
                                        
            if($this->social_model->where(['list_id' => $vendor['id'], 'type' => 4])->get() != FALSE)
                $this->social_model->update(['url' => $this->input->post('website')], ['list_id' => $vendor['id'], 'type' => 4]);
            else
                $this->social_model->insert(['list_id' => $vendor['id'], 'url' => $this->input->post('website'), 'type' => 4]);
                                                
            }
            $this->set_response_simple(NULL, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
        } elseif ($type == 'cover_and_banner_images') {
            $banners = $this->input->post('banners');
            file_put_contents("./uploads/list_cover_image/list_cover_" . $vendor['id'] . ".jpg", base64_decode($this->input->post('cover_image')));
            if(! empty($banners)){foreach ($banners as $banner){
                if(! empty($banner['id'])){
                    if (! file_exists('uploads/' . 'list_banner' . '_image/')) {
                        mkdir('uploads/' . 'list_banner' . '_image/', 0777, true);
                    }
                    if (file_exists(base_url() ."uploads/list_banner_image/list_banner_" . $banner['id']. ".jpg")) {
                        unlink(base_url() ."uploads/list_banner_image/list_banner_" .$banner['id']. ".jpg");
                    }
                    file_put_contents("uploads/list_banner_image/list_banner_".$banner['id'].".jpg", base64_decode($banner['image']));
                }else{
                    $image_id = $this->vendor_banner_model->insert([
                        'list_id' => $vendor['id'],
                        'image' => 'banner_'.$this->input->post('id').'.jpg',
                        'ext' => 'jpg'
                    ]);
                    if (! file_exists('uploads/' . 'list_banner' . '_image/')) {
                        mkdir('uploads/' . 'list_banner' . '_image/', 0777, true);
                    }
                    if (file_exists(base_url() ."uploads/list_banner_image/list_banner_" . $image_id. ".jpg")) {
                        unlink(base_url() ."uploads/list_banner_image/list_banner_" .$image_id. ".jpg");
                    }
                    file_put_contents("uploads/list_banner_image/list_banner_$image_id.jpg", base64_decode($banner['image']));
                }
            }}
            $this->set_response_simple(NULL, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
        }elseif ($type == 'delete_banner') {
            $this->vendor_banner_model->delete([
                'id' => $this->input->post('id')
            ]);
            if (file_exists(base_url() ."uploads/list_banner_image/list_banner_" . $this->input->post('id') . ".jpg")) {
                unlink(base_url() ."uploads/list_banner_image/list_banner_" .$this->input->post('id') . ".jpg");
            }
            $this->set_response_simple(NULL, 'Banner deleted..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * To get list of packages available
     *
     * @author Mehar
     *
     */
    public function packages_get(){
        $packages = $this->package_model->get_all();
        $this->set_response_simple($packages, 'List of packages..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    /**
     * To get list of Keads
     *
     * @author Mehar
     *
     */
    public function leads_get(){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $vendor_leads = $this->vendor_leads_model->order_by('id', 'DESC')->with_lead('fields: id, user_id')->where('vendor_id', $token_data->id)->get_all();
        if(! empty($vendor_leads)){foreach($vendor_leads as $key => $lead){
            $vendor_leads[$key]['lead']['user'] = $this->user_model->fields('unique_id, email, first_name, last_name, phone')->get($lead['lead']['user_id']);
        }}
        $this->set_response_simple($vendor_leads, 'List of Leads..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    /**
     * To get list of packages available
     *
     * @author Mehar
     *
     * @param string $type
     */
    
    public function settings_post($type = 'r'){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        $this->vendor_settings_model->user_id = $token_data->id;
        if($type == 'r'){
            $data['shop_settings'] = $this->food_settings_model->where('vendor_id', $token_data->id)->get();
            $this->set_response_simple($data, 'Settings..!', REST_Controller::HTTP_OK, TRUE);
        }elseif($type == 'u'){
            $this->form_validation->set_rules($this->vendor_settings_model->rules['food']);
            if ($this->form_validation->run() == FALSE) {//echo validation_errors();
                $this->set_response_simple(validation_errors(), "Vallidation errors", REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $settings = $this->food_settings_model->fields('id')->where('vendor_id',$token_data->id)->get();
                if(! empty($settings)){
                    $this->food_settings_model->update([
                        'min_order_price' => $this->input->post('min_order_price'),
                        'min_delivery_fee' => $this->input->post('min_delivery_fee'),
                        'ext_delivery_fee' => $this->input->post('ext_delivery_fee'),
                        'delivery_free_range' => $this->input->post('delivery_free_range'),
                        'label' => $this->input->post('label'),
                        'tax' => $this->input->post('tax'),
                    ], ['vendor_id'=>$token_data->id]);
                    $this->set_response_simple(NULL, 'Settings updated..!', REST_Controller::HTTP_CREATED, TRUE);
                }else{
                    $this->food_settings_model->insert([
                        'min_order_price' => $this->input->post('min_order_price'),
                        'delivery_free_range' => $this->input->post('delivery_free_range'),
                        'min_delivery_fee' => $this->input->post('min_delivery_fee'),
                        'ext_delivery_fee' => $this->input->post('ext_delivery_fee'),
                        'label' => $this->input->post('label'),
                        'tax' => $this->input->post('tax'),
                        'vendor_id'=>$token_data->id
                    ]);
                    $this->set_response_simple(NULL, 'Settings created..!', REST_Controller::HTTP_CREATED, TRUE);
                }
            }
        } 
    }
    /**
     * To manage profile
     *
     * @author Mehar
     *        
     * @param string $type
     */
    public function subscriptions_post($type = 'list_of_subscriptions')
    {
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        if(empty($this->input->post('service_id'))){
            $this->set_response_simple(NULL, 'Please provide service_id.', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
        }else{
            if($type == 'verify'){
                $vendor_package = $this->vendor_package_model->where(['created_user_id' => $token_data->id, 'status' => 1])->get();
                if(! empty($vendor_package)){
                    if($vendor_package['id'] == 1){
                        $vendor_package['subscribed_at'] = $vendor_package['created_at'];
                        $vendor_package['expires_at'] = date('Y-m-d', strtotime($vendor_package['subscribed_at']. ' + '.$vendor_package['package']['days'].' days'));
                        $this->set_response_simple($vendor_package, 'Success..!', REST_Controller::HTTP_OK, TRUE);
                    }else{
                        $active_package = $this->vendor_package_model->with_package('fields: id, title, desc, days', 'where: service_id = '.$this->input->post('service_id'))->where(['status' => 1, 'created_user_id' => $token_data->id])->get();
                        if(! empty($active_package)){
                            $active_package['subscribed_at'] = $active_package['created_at'];
                            $active_package['expires_at'] = date('Y-m-d', strtotime($active_package['subscribed_at']. ' + '.$active_package['package']['days'].' days'));
                            $this->set_response_simple($active_package, 'Success..!', REST_Controller::HTTP_OK, TRUE);
                        }else{ 
                            $this->set_response_simple($vendor_package, 'No Active subscriptions..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
                        }
                    }
                }else{
                    $this->set_response_simple($vendor_package, 'No subscriptions..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
                }
                
            }elseif ($type == 'c'){
                $active_package = $this->vendor_package_model->with_package('fields: id, title, desc, days', 'where: service_id = '.$this->input->post('service_id'))->where(['package_id' => $this->input->post('package_id'), 'created_user_id' => $token_data->id])->get();
                if(empty($active_package)){ 
                    $this->vendor_package_model->update([
                        'status' => 2,
                        'service_id' => (empty($this->input->post('service_id')))? NULL : $this->input->post('service_id'),
                        'created_user_id' => $token_data->id,
                        'updated_user_id' => $token_data->id,
                        'updated_at' => date('Y-m-d H:i:s')
                    ], ['created_user_id', 'service_id']);
                    $id = $this->vendor_package_model->insert([
                        'package_id' => $this->input->post('package_id'),
                        'service_id' => (empty($this->input->post('service_id')))? NULL : $this->input->post('service_id'),
                        'created_user_id' => $token_data->id,
                        'status' => 1
                    ]);
                    $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_OK, TRUE);
                 }else {
                    $active_package['subscribed_at'] = $active_package['created_at'];
                    $active_package['expires_at'] = date('Y-m-d', strtotime($active_package['subscribed_at']. ' + '.$active_package['package']['days'].' days'));
                    $this->set_response_simple($active_package, 'You already have subscribed.', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
                } 
            }elseif ($type == 'list_of_subscriptions'){
                $active_package = $this->vendor_package_model->with_package('fields: id, title, desc, days')->where(['created_user_id' => $token_data->id, 'service_id' => $this->input->post('service_id')])->get();
                if(! empty($active_package)){
                    $active_package['subscribed_at'] = $active_package['created_at'];
                    $active_package['expires_at'] = date('Y-m-d', strtotime($active_package['subscribed_at']. ' + '.$active_package['package']['days'].' days'));
                }
                $this->set_response_simple($active_package, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            }elseif ($type == 'all_plans'){
                $packages['all_plans'] = $this->package_model->where(['service_id' => $this->input->post('service_id')])->fields('id, title, desc, price, days')->get_all();
                $packages['active_plan'] = $this->vendor_package_model->with_package('fields: id, title, desc, days')->where(['created_user_id' => $token_data->id, 'service_id' => $this->input->post('service_id')])->get();
                if(! empty($packages['active_plan'])){
                    $packages['active_plan']['subscribed_at'] = $packages['active_plan']['created_at'];
                    $packages['active_plan']['expires_at'] = date('Y-m-d', strtotime($packages['active_plan']['subscribed_at']. ' + '.$packages['active_plan']['package']['days'].' days'));
                }
                $this->set_response_simple((! empty($packages)) ? $packages : NULL, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            }
        }
    }
}