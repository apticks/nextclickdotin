<?php if(!empty($this->session->flashdata('request_success'))){ ?>

    <script type='text/javascript'>
        window.alert('Successfully Subscribed!!!')
    </script>

    <?php }?>
    <div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4 class="col-10 ven1">List of Requests</h4>
					<a class="btn btn-outline-dark btn-lg col-2" href="<?php echo base_url('request/c')?>"><i class="fa fa-plus" aria-hidden="true"></i> Add Request's</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Request Title</th>
									<th>Description</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($request)):?>
    							<?php  $sno = 1; foreach ($request as $faq_obj): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									 <td><?php echo $faq_obj['title'];?></td>
    								<td><?php echo $faq_obj['desc'];?></td>
									<td><a
										href="<?php echo base_url()?>request/edit?id=<?php echo $faq_obj['id']; ?>"
										class=" mr-2  " type="request"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $faq_obj['id'] ?>, 'request')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='5'><h3>
											<center>Sorry!! No Request's!!!</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>