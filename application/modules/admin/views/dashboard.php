
<?php if($this->ion_auth_acl->has_permission('admin') || $this->ion_auth_acl->has_permission('hr')){?>
<h3>Users</h3>
<div class="row">
	<div class="col-xl-3  col-md-4 col-sm-4">
		<a href="<?php echo base_url('employee/r/0');?>">
		<div class="card">
			<div class="card-bg">
				<div class="p-t-20 d-flex justify-content-between">
					<div class="col">
						<h6 class="mb-0">All users</h6>
					</div>
					<!-- <i class="fas fa-address-card card-icon col-orange font-30 p-r-30"></i> -->
				</div>
				<!-- <canvas id="cardChart1" height="80"></canvas> -->
				<br/>
					<div class="alert alert-sm alert-primary "><center><i class="fas fa-user-circle card-icon font-20 p-r-30">  <?php echo $this->db->query('SELECT COUNT(*) AS `no_of_users` FROM `users` as u WHERE u.deleted_at is null')->row()->no_of_users;?></i></center></div>
			</div>
		</div>
		</a>
	</div>
	<div class="col-xl-3  col-md-4 col-sm-4">
	 <a href="<?php echo base_url('vendors_filter/0');?>">
		<div class="card">
			<div class="card-bg">
				<div class="p-t-20 d-flex justify-content-between">
					<div class="col">
						<h6 class="mb-0">Vendors</h6>
					</div>
				</div>
				<br/>
			<div class="alert alert-sm alert-primary "><center><b><i class="fas fa-check-circle card-icon font-20 p-r-30 "  title="Active Vendors" > <?php echo $this->db->query('SELECT COUNT(*) AS active FROM vendors_list WHERE status=1 and deleted_at is null')->row()->active;?></i></b></<br><b><i class="fas fa-times-circle card-icon font-20 p-r-30" title="Inactive Vendors"> <?php echo $this->db->query('SELECT COUNT(*) AS inactive FROM vendors_list WHERE status=2')->row()->inactive;?></i></b></center></div>
			</div>
		</div>
		</a>
	</div>
	<div class="col-xl-3  col-md-4 col-sm-4">
		<a href="<?php echo base_url('employee/r/0');?>">
		<div class="card">
			<div class="card-bg">
				<div class="p-t-20 d-flex justify-content-between">
					<div class="col">
						<h6 class="mb-0">HR</h6>
					</div>
					<!-- <i class="fas fa-address-card card-icon col-orange font-30 p-r-30"></i> -->
				</div>
				<!-- <canvas id="cardChart1" height="80"></canvas> -->
				<br/>
					<div class="alert alert-sm alert-primary "><center><i class="fas fa-user-circle card-icon font-20 p-r-30">  <?php echo $this->db->query('SELECT COUNT(*) AS `no_of_hrs` FROM `users` as u LEFT JOIN users_groups as ug ON u.id = ug.user_id WHERE u.deleted_at is null and ug.group_id = (SELECT id FROM `groups` WHERE name = "hr")')->row()->no_of_hrs;?></i></center></div>
			</div>
		</div>
		</a>
	</div>
	<div class="col-xl-3  col-md-4 col-sm-4">
		<a href="<?php echo base_url('employee/r/0');?>">
		<div class="card">
			<div class="card-bg">
				<div class="p-t-20 d-flex justify-content-between">
					<div class="col">
						<h6 class="mb-0">Divisional Heads</h6>
					</div>
					<!-- <i class="fas fa-address-card card-icon col-orange font-30 p-r-30"></i> -->
				</div>
				<!-- <canvas id="cardChart1" height="80"></canvas> -->
				<br/>
				<div class="alert alert-sm alert-primary "><center><i class="fas fa-user-circle card-icon font-20 p-r-30">  <?php echo $this->db->query('SELECT COUNT(*) AS `no_of_dhs` FROM `users` as u LEFT JOIN users_groups as ug ON u.id = ug.user_id WHERE u.deleted_at is null and ug.group_id = (SELECT id FROM `groups` WHERE name = "dh")')->row()->no_of_dhs;?></i></center></div>
			</div>
		</div>
		</a>
	</div>
</div>
<hr class="dashboard-hr"/>
<h3>Ecommerce</h3>
<div class="row">
    <div class="col-xl-4  col-md-4 col-sm-4">
	 <a href="<?php echo base_url('products_approve/r');?>">
		<div class="card">
			<div class="card-bg">
				<div class="p-t-20 d-flex justify-content-between">
					<div class="col">
						<h6 class="mb-0">Vendor Products</h6>
					</div>
				</div>
				<br/>
			<div class="alert alert-sm alert-primary "><center><b><i class="fas fa-check-circle card-icon font-20 p-r-30 "  title="Active Vendors" > <?php echo $this->db->query('SELECT COUNT(*) AS active FROM food_item WHERE status=1 and deleted_at is null')->row()->active;?></i></b></<br><b><i class="fas fa-times-circle card-icon font-20 p-r-30" title="Inactive Vendors"> <?php echo $this->db->query('SELECT COUNT(*) AS inactive FROM food_item WHERE status=2')->row()->inactive;?></i></b></center></div>
			</div>
		</div>
		</a>
	</div>
     
    <div class="col-xl-3  col-md-4 col-sm-4">
    	<a href="<?php echo base_url('food_orders/r');?>">
    		<div class="card">
    			<div class="card-bg">
    				<div class="p-t-20 d-flex justify-content-between">
    					<div class="col">
    						<h6 class="mb-0">Overall sales</h6>
    						<span class="font-weight-bold mb-0 font-20"></span>
    					</div>
    				</div>
    				<!-- <canvas id="cardChart4" height="80"></canvas> -->
    				<br/>
    				<div class="alert alert-sm alert-primary "><center><i class="fas fa-chart-bar card-icon font-20 p-r-30">  <?php echo number_format((float)$this->db->query('SELECT SUM(total) AS total FROM `food_orders` WHERE `order_status` = 6')->row()->total, 2, '.', '');?>₹</i></center></div>
    			</div>
    		</div>
    	</a>
    </div>
    <div class="col-xl-3  col-md-4 col-sm-4">
    	<a href="<?php echo base_url('food_orders/r');?>">
    		<div class="card">
    			<div class="card-bg">
    				<div class="p-t-20 d-flex justify-content-between">
    					<div class="col">
    						<h6 class="mb-0">Today sales</h6>
    						<span class="font-weight-bold mb-0 font-20"></span>
    					</div>
    				</div>
    				<!-- <canvas id="cardChart4" height="80"></canvas> -->
    				<br/>
    				<div class="alert alert-sm alert-primary "><center><i class="fas fa-chart-bar card-icon font-20 p-r-30">  <?php echo ($this->db->query('SELECT SUM(total) AS total FROM `food_orders` WHERE CURRENT_DATE = DATE(created_at) AND order_status = 6')->row()->total)? number_format((float)$this->db->query('SELECT SUM(total) AS total FROM `food_orders` WHERE CURRENT_DATE = DATE(created_at) AND order_status = 6')->row()->total, 2, '.', ''): 0;?>₹</i></center></div>
    			</div>
    		</div>
    	</a>
    </div>
</div>
<hr class="dashboard-hr"/>
<h3>Doctors</h3>
<div class="row">
 	<div class="col-xl-4  col-md-4 col-sm-4">
	 <a href="<?php echo base_url('doctors_approve/r');?>">
		<div class="card">
			<div class="card-bg">
				<div class="p-t-20 d-flex justify-content-between">
					<div class="col">
						<h6 class="mb-0">Vendor Doctors</h6>
					</div>
				</div>
				<br/>
			<div class="alert alert-sm alert-primary "><center><b><i class="fas fa-check-circle card-icon font-20 p-r-30 "  title="Active Vendors" > <?php echo $this->db->query('SELECT COUNT(*) AS active FROM hosp_doctors_details WHERE deleted_at is null and  status=1')->row()->active;?></i></b></<br><b><i class="fas fa-times-circle card-icon font-20 p-r-30" title="Inactive Vendors"> <?php echo $this->db->query('SELECT COUNT(*) AS inactive FROM hosp_doctors_details WHERE deleted_at is null and  status=2')->row()->inactive;?></i></b></center></div>
			</div>
		</div>
		</a>
	</div>
    
    <div class="col-xl-4  col-md-4 col-sm-4">
    	<a href="<?php echo base_url('doctors_booking/r');?>">
    		<div class="card">
    			<div class="card-bg">
    				<div class="p-t-20 d-flex justify-content-between">
    					<div class="col">
    						<h6 class="mb-0">Overall sales</h6>
    						<span class="font-weight-bold mb-0 font-20"></span>
    					</div>
    				</div>
    				<!-- <canvas id="cardChart4" height="80"></canvas> -->
    				<br/>
    				<div class="alert alert-sm alert-primary "><center><i class="fas fa-chart-bar card-icon font-20 p-r-30">  <?php echo number_format((float)$this->db->query('SELECT SUM(b.total) as total FROM `booking_items` as bi JOIN bookings as b on b.id = bi.booking_id where bi.service_id = 11 AND b.booking_status = 4')->row()->total, 2, '.', '');?>₹</i></center></div>
    			</div>
    		</div>
    	</a>
    </div>
    
     <div class="col-xl-4  col-md-4 col-sm-4">
    	<a href="<?php echo base_url('doctors_booking/r');?>">
    		<div class="card">
    			<div class="card-bg">
    				<div class="p-t-20 d-flex justify-content-between">
    					<div class="col">
    						<h6 class="mb-0">Today sales</h6>
    						<span class="font-weight-bold mb-0 font-20"></span>
    					</div>
    				</div>
    				<!-- <canvas id="cardChart4" height="80"></canvas> -->
    				<br/>
    				<div class="alert alert-sm alert-primary "><center><i class="fas fa-chart-bar card-icon font-20 p-r-30">  <?php echo ($this->db->query('SELECT SUM(total) AS total FROM `food_orders` WHERE CURRENT_DATE = DATE(created_at) AND order_status = 6')->row()->total)? number_format((float)$this->db->query('SELECT SUM(b.total) as total FROM `booking_items` as bi JOIN bookings as b on b.id = bi.booking_id where bi.service_id = 11 AND b.booking_status = 4 AND CURRENT_DATE = DATE(bi.created_at)')->row()->total, 2, '.', ''): 0;?>₹</i></center></div>
    			</div>
    		</div>
    	</a>
    </div>
</div>
<hr class="dashboard-hr"/>
<h3>On Demand Services</h3>
<div class="row">
	<div class="col-xl-4  col-md-4 col-sm-4">
	 <a href="<?php echo base_url('od_categories_approve/r');?>">
		<div class="card">
			<div class="card-bg">
				<div class="p-t-20 d-flex justify-content-between">
					<div class="col">
						<h6 class="mb-0">On Demand Services</h6>
					</div>
				</div>
				<br/>
			<div class="alert alert-sm alert-primary "><center><b><i class="fas fa-check-circle card-icon font-20 p-r-30 "  title="Active Vendors" > <?php echo $this->db->query('SELECT COUNT(*) AS active FROM od_services_details WHERE deleted_at is null and status=1')->row()->active;?></i></b></<br><b><i class="fas fa-times-circle card-icon font-20 p-r-30" title="Inactive Vendors"> <?php echo $this->db->query('SELECT COUNT(*) AS inactive FROM od_services_details WHERE deleted_at is null and  status=2')->row()->inactive;?></i></b></center></div>
			</div>
		</div>
		</a>
	</div>
    
    <div class="col-xl-4  col-md-4 col-sm-4">
    	<a href="<?php echo base_url('services_booking/r');?>">
    		<div class="card">
    			<div class="card-bg">
    				<div class="p-t-20 d-flex justify-content-between">
    					<div class="col">
    						<h6 class="mb-0">Overall sales</h6>
    						<span class="font-weight-bold mb-0 font-20"></span>
    					</div>
    				</div>
    				<!-- <canvas id="cardChart4" height="80"></canvas> -->
    				<br/>
    				<div class="alert alert-sm alert-primary "><center><i class="fas fa-chart-bar card-icon font-20 p-r-30">  <?php echo number_format((float)$this->db->query('SELECT SUM(b.total) as total FROM `booking_items` as bi JOIN bookings as b on b.id = bi.booking_id where bi.service_id = 8 AND b.booking_status = 4')->row()->total, 2, '.', '');?>₹</i></center></div>
    			</div>
    		</div>
    	</a>
    </div>
    
     <div class="col-xl-4  col-md-4 col-sm-4">
    	<a href="<?php echo base_url('services_booking/r');?>">
    		<div class="card">
    			<div class="card-bg">
    				<div class="p-t-20 d-flex justify-content-between">
    					<div class="col">
    						<h6 class="mb-0">Today sales</h6>
    						<span class="font-weight-bold mb-0 font-20"></span>
    					</div>
    				</div>
    				<!-- <canvas id="cardChart4" height="80"></canvas> -->
    				<br/>
    				<!-- <div class="alert alert-sm alert-primary "><center><i class="fas fa-chart-bar card-icon font-20 p-r-30">  <?php //echo ($this->db->query('SELECT SUM(b.total) as total FROM `booking_items` as bi JOIN bookings as b on b.id = bi.booking_id where bi.service_id = 8 AND b.booking_status = 4 AND CURRENT_DATE = DATE(bi.created_at)')->row()->total)? number_format((float)$this->db->query('SELECT SUM(total) AS total FROM `booking_items` WHERE CURRENT_DATE = DATE(created_at) AND booking_status = 6')->row()->total, 2, '.', ''): 0;?>₹</i></center></div> -->
    				<div class="alert alert-sm alert-primary "><center><i class="fas fa-chart-bar card-icon font-20 p-r-30">  <?php echo ($this->db->query('SELECT SUM(total) AS total FROM `food_orders` WHERE CURRENT_DATE = DATE(created_at) AND order_status = 6')->row()->total)? number_format((float)$this->db->query('SELECT SUM(b.total) as total FROM `booking_items` as bi JOIN bookings as b on b.id = bi.booking_id where bi.service_id = 8 AND b.booking_status = 4 AND CURRENT_DATE = DATE(bi.created_at)')->row()->total, 2, '.', ''): 0;?>₹</i></center></div>

    			</div>
    				    			</div>
    		</div>
    	</a>
    </div>
</div>
<?php }else{?>
	<h1>Hello <?php echo $user->first_name.' '.$user->last_name?>, Welcome to Nextclick</h1>
<?php }?>
						