<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Dashboard.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Dashboard extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        
        if (! $this->ion_auth->logged_in())
            redirect('auth/login');
        
            $this->load->model('vendor_list_model');
            $this->load->model('wallet_transaction_model');
            $this->load->model('vendor_leads_model');
            $this->load->model('user_model');
    }

    public function index()
    {
        $this->data['users_groups']           =   $this->ion_auth->get_users_groups()->result();
        $this->data['users_permissions']      =   $this->ion_auth_acl->build_acl();
        $this->data['title'] = 'Dashboard';
        $this->data['content'] = 'admin/dashboard';
        $this->data['nav_type'] = 'dashboard';
        $this->_render_page($this->template, $this->data);
    }
    
    public function sample(){
        $this->data['title'] = 'Sample';
        $this->data['content'] = 'admin/sample';
        $this->_render_page($this->template, $this->data);
    }
    
    public function wallet(){
        $this->data['title'] = 'Admin Wallet Transactions';
        $this->data['content'] = 'admin/admin/wallet';
        $this->data['transactions'] = $this->wallet_transaction_model->where('user_id', $this->ion_auth->get_user_id() )->order_by('id', 'DESC')->get_all();
        $this->_render_page($this->template, $this->data);
    }
    
    public function lead_management($type = 'r'){
        if($type == 'r'){
            $this->data['title'] = 'Sample';
            $this->data['content'] = 'admin/admin/lead_management';
            $this->data['vendor_leads'] = $this->vendor_leads_model->order_by('id', 'DESC')->with_lead('fields: id, user_id')->where('vendor_id', $this->ion_auth->get_user_id())->get_all();
            foreach($this->data['vendor_leads'] as $key => $lead){
                $this->data['vendor_leads'][$key]['lead']['user'] = $this->user_model->get($lead['lead']['user_id']);
            }
            $this->_render_page($this->template, $this->data);
        }
    }
    

}
?>