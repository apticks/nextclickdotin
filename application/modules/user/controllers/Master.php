<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;

class Master extends MY_REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('vendor_list_model');
        $this->load->model('news_model');
        $this->load->model('users_address_model');
        $this->load->model('location_model');
        $this->load->model('setting_model');
        $this->load->model('user_service_model');
        $this->load->model('vendor_banner_model');
        $this->load->model('user_model');
        $this->load->model('setting_model');
        $this->load->model('hosp_speciality_model');
        $this->load->model('hosp_doctor_model');
        $this->load->model('od_category_model');
        $this->load->model('od_service_model');
    }

    /**
     * To get list of vendor depends upon category, search & near by location
     *
     * @author Mehar
     * @param integer $limit
     * @param integer $offset
     * @param integer $cat_id
     */
    public function vendor_list_get($limit = 10, $offset = 0)
    {
            $data = $this->vendor_list_model->all($limit, $offset, (isset($_GET['cat_id'])) ? $this->input->get('cat_id') : NUll, (isset($_GET['sub_cat_id'])) ? $this->input->get('sub_cat_id') : NUll, (isset($_GET['q'])) ? $this->input->get('q') : NUll, (isset($_GET['latitude'])) ? $this->input->get('latitude') : NUll, (isset($_GET['longitude'])) ? $this->input->get('longitude') : NUll, (isset($_GET['brand_id'])) ? $this->input->get('brand_id') : NUll);
            if (! empty($data['result'])) {
                foreach ($data['result'] as $d) {
                    $d->avg_rating = $this->db->query('SELECT AVG(rating) as avg_rating FROM `vendor_ratings` WHERE list_id ='.$d->id)->row()->avg_rating;
                    $d->image = base_url() . 'uploads/list_cover_image/list_cover_' . $d->id . '.jpg'.'?'.time();
                }
            }
            $this->set_response_simple((empty($data['result'])) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }

    /**
     * User address
     *
     * To Manage address
     *
     * @author Trupti
     * @param string $type
     */
    public function user_address_post($type = 'r')
    {
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        if ($type == 'c') {
            $this->form_validation->set_rules($this->users_address_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $v = $this->location_model->where('latitude', $this->input->post('latitude'))
                    ->where('longitude', $this->input->post('longitude'))
                    ->get();
                if ($v != '') {
                    $l_id = $v['id'];
                } else {
                    $l_id = $this->location_model->insert([
                        'latitude' => $this->input->post('latitude'),
                        'longitude' => $this->input->post('longitude'),
                        'address' => $this->input->post('address')
                    ]);
                }

                $id = $this->users_address_model->insert([
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'location_id' => $l_id
                ]);
                $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }
        } elseif ($type == 'r') {
            $lat = $this->input->post('latitude');
            $long = $this->input->post('longitude');
            if(! is_null($lat) && ! is_null($long)){
                $locations = $this->db->query("SELECT id, ( 3959 * acos( cos( radians($lat) ) * cos( radians( locations.latitude ) ) * cos( radians( locations.longitude ) - radians($long) ) + sin( radians($lat) ) * sin(radians(locations.latitude)) ) ) AS distance FROM locations HAVING distance < 3.16 ORDER BY distance")->result_array();
                $location_ids = (empty(array_column($locations, 'id')))? 0: implode(',', array_column($locations, 'id'));
            }
            $data = $this->db->query("SELECT `users_address`.`id`, `users_address`. `user_id`, `users_address`. `name`, `users_address`. `phone`, `users_address`. `email`, `users_address`. `address`, `users_address`. `location_id` FROM `users_address` WHERE `users_address`.`user_id` = ".$token_data->id." AND users_address.deleted_at IS NULL AND `users_address`.`location_id` IN (".$location_ids.")")->result_array();
            $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        } elseif ($type == 's') {
            $data = $this->users_address_model->fields('id, user_id, name, phone, email, address, location_id')->get('id', $this->input->post('id'));
            $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        } elseif ($type == 'u') {
            $this->form_validation->set_rules($this->users_address_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $v = $this->location_model->where('latitude', $this->input->post('latitude'))
                    ->where('longitude', $this->input->post('longitude'))
                    ->get();
                if ($v != '') {
                    $l_id = $v['id'];
                } else {
                    $l_id = $this->location_model->insert([
                        'latitude' => $this->input->post('latitude'),
                        'longitude' => $this->input->post('longitude'),
                        'address' => $this->input->post('address')
                    ]);
                }
                $ll = $this->users_address_model->update([
                    'id' => $this->input->post('id'),
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'location_id' => $l_id
                ], 'id');
                $this->set_response_simple($ll, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
            }
        } elseif ($type == 'd') {
            $ll = $this->users_address_model->delete([
                'id' => $this->input->post('id')
            ]);
            $this->set_response_simple($ll, 'Deleted..!', REST_Controller::HTTP_OK, TRUE);
        }
    }

    /**
     * To get the individual vendor details
     *
     * @author Mehar
     * @param number $target
     */
    public function vendor_get($target = 1)
    {
        $data = $this->vendor_list_model->fields('id, vendor_user_id, name, desc, landmark, email, unique_id, created_at, status, availability')
            ->with_timings('fields: list_id, start_time, end_time')
            ->with_location('fields: id, address, latitude, longitude')
            ->with_category('fields: id, name, status')
            ->with_sub_categories('fields: id, name, status')
            ->with_constituency('fields: id, name, state_id, district_id')
            ->with_contacts('fields: id, std_code, number, type')
            ->with_links('fields: id, url, type')
            ->with_amenities('fields: id, name')
            ->with_ratings('fields: id, user_id, rating, review')
            ->with_services('fields: id, name')
            ->with_specialities('fields: id, name, desc')
            ->with_on_demand_categories('fields: id, name, desc')
            ->with_holidays('fields: id')
            ->where('id', $target)
            ->get(); 
            $shop_by_categoris = $this->db->query("SELECT sc.id, sc.name, sc.type, sc.desc FROM shop_by_categories as sbc join sub_categories as sc on sc.id = sbc.sub_cat_id where vendor_id = ".$data['vendor_user_id']." and sub_cat_id not in(select sub_cat_id from vendor_in_active_shop_by_categories where vendor_id = ".$data['vendor_user_id']." ) and sc.status = 1 and sc.deleted_at is null")->result_array();
            $data['sub_categories'] = (! empty($shop_by_categoris)) ? $shop_by_categoris : NULL;
            $data['specialities'] = is_array($data['specialities'])?array_values($data['specialities']) : NULL;
            $data['on_demand_categories'] = is_array($data['on_demand_categories'])?array_values($data['on_demand_categories']) : NULL;
            $data['services'] = is_array($data['services'])? array_values($data['services']) : NULL;
            $data['shop_by_categories'] = $this->db->query("SELECT sc.id, sc.cat_id, sc.type, sc.name, sc.desc, sc.status, sbc.vendor_id FROM `shop_by_categories` AS sbc JOIN sub_categories AS sc ON sbc.sub_cat_id = sc.id WHERE sbc.`vendor_id` IN (1,".$data['vendor_user_id'].") AND sc.type = 2 AND sc.status = 1 AND sbc.cat_id=".$data['category_id']." AND sbc.sub_cat_id NOT IN(SELECT sub_cat_id FROM `vendor_in_active_shop_by_categories` WHERE vendor_id = ".$data['vendor_user_id'].")")->result_array();
            $data['category']['coming_soon_image'] = base_url(). 'uploads/coming_soon_image/coming_soon_'.$data['category']['id'].'.jpg'.'?'.time();
        $data['user_services'] = $this->user_service_model->order_by('id', 'DESC')
            ->where('created_user_id', $data['vendor_user_id'])
            ->get_all();
        if ($data != FALSE) {
            $vendor_banners = $this->vendor_banner_model->where('list_id', $data['id'])->get_all();
            $data['banners'] = [];
            if ($vendor_banners) {
                foreach ($vendor_banners as $key => $banner) {
                    $data['banners'][$key] = base_url() . "uploads/list_banner_image/list_banner_" . $banner['id'] . ".jpg".'?'.time();
                }
            }
            if (! empty($data['sub_categories'])) {
                for ($i = 0; $i < count($data['sub_categories']); $i ++) {
                    $data['sub_categories'][$i]['image'] = base_url() . 'uploads/sub_category_image/sub_category_' . $data['sub_categories'][$i]['id'] . '.jpg'.'?'.time();
                }
            }
            if (! empty($data['shop_by_categories'])) {
                for ($i = 0; $i < count($data['shop_by_categories']); $i ++) {
                    $data['shop_by_categories'][$i]['image'] = base_url() . 'uploads/sub_category_image/sub_category_' . $data['shop_by_categories'][$i]['id'] . '.jpg'.'?'.time();
                }
            }
            if (! empty($data['services'])) {
                for ($i = 0; $i < count($data['services']); $i ++) {
                    $data['services'][$i]['image'] = base_url() . 'uploads/service_image/service_' . $data['services'][$i]['id'] . '.jpg'.'?'.time();
                }
            }
            if (! empty($data['specialities'])) {
                for ($i = 0; $i < count($data['specialities']); $i ++) {
                    $data['specialities'][$i]['image'] = base_url() . 'uploads/speciality_image/speciality_' . $data['specialities'][$i]['id'] . '.jpg'.'?'.time();
                }
            }
            if (! empty($data['on_demand_categories'])) {
                for ($i = 0; $i < count($data['on_demand_categories']); $i ++) {
                    $data['on_demand_categories'][$i]['image'] = base_url() . 'uploads/od_category_image/od_category_' . $data['on_demand_categories'][$i]['id'] . '.jpg'.'?'.time();
                }
            }
            if (! empty($data['ratings'])) {
                for ($i = 0; $i < count($data['ratings']); $i ++) {
                    $data['ratings'][$i]['user'] = $this->user_model->fields('id, first_name, unique_id')->get($data['ratings'][$i]['user_id']);
                }
            }
            $data['cover'] = base_url() . "uploads/list_cover_image/list_cover_" . $data['id'] . ".jpg".'?'.time();
            $field_where="(`desc` = 'app_ord_label' OR `desc` = 'app_ord_quantity' OR `desc` = 'app_ord_address')";
            $data['fields']=$this->db->where($field_where)->select('acc_id,name,desc,field_status')->get_where('manage_account_names',array('status'=>1,'category_id'=>$data['category_id']))->result();
           
        
        }
        $this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }

    /**
     * To get the news
     *
     * @author Mehar
     * @param number $limit,offset
     */
    public function news_get($limit = 10, $offset = 0)
    {
        $data = $this->news_model->all($limit, $offset);
        if (! empty($data['result'])) {
            foreach ($data['result'] as $d) {
                $d->image = base_url() . 'uploads/news_image/news_' . $d->id . '.jpg';
            }
        }
        $this->set_response_simple((empty($data['result'])) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }

    /**
     * To get the User Dettails
     *
     * @author Mahesh
     *        
     */
    public function user_details_get()
    {
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $this->load->model('user_model');
        $result = $this->user_model->order_by('id', 'DESC')
            ->fields('id,unique_id,first_name,last_name,email,phone, wallet')
            ->with_groups('fields:name,id')
            ->where('id', $token_data->id)
            ->get();
        $this->set_response_simple(($result == FALSE) ? FALSE : $result, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    /**
     * @desc Manage profile
     * 
     * @author Mehar
     */
    public function profile_post($type = 'r'){

        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        if($type == 'r'){
            $data = $this->user_model
            ->fields('id,unique_id,first_name,last_name,email,phone, wallet')
            ->with_groups('fields:name,id')
            ->where('id', $token_data->id)
            ->get();
            $data['image'] = base_url() . 'uploads/profile_image/profile_' . $data['unique_id'] . '.jpg';
            $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif($type == 'u'){
            $is_updated = $this->user_model->update([
                'id' => $token_data->id,
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
            ], 'id');
            if (!file_exists(base_url().'uploads/profile_image/')) {
                mkdir('uploads/profile_image/', 0777, true);
            }
            unlink('uploads/' . 'profile_image' . '_image/' . 'profile' . '_' . $this->input->post('unique_id') . '.jpg');
            file_put_contents("./uploads/profile_image/profile_".$this->input->post('unique_id').".jpg", base64_decode($this->input->post('image')));
            if($is_updated){
                $this->set_response_simple(($is_updated == FALSE) ? FALSE : $is_updated, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
            }else {
                $this->set_response_simple(($is_updated == FALSE) ? FALSE : $is_updated, 'Failed..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, TRUE);
            }
        }
    }


    public function payment_settings_get()
    {
        $result['pay_per_vendor'] = $this->setting_model->where('key', 'pay_per_vendor')->get()['value'];
        $result['vendor_validation'] = $this->setting_model->where('key', 'vendor_validation')->get()['value'];
        $this->set_response_simple(($result == FALSE) ? FALSE : $result, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }

    /**
     * Vendor Lead Generation
     *
     * To Manage Lead Generation
     *
     * @author Mahesh
     * @param string $type
     */
    public function LeadGeneration_post($type = 'c')
    {
        $this->load->model('vendor_leads_model');
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if ($type == 'c') {
            $id = $this->vendor_leads_model->insert([
                'user_id' => $token_data->id,
                'vendor_id' => $this->input->post('vendor_id'),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $this->set_response_simple($id, 'We Will Contact You Soon', REST_Controller::HTTP_CREATED, TRUE);
        }
        if ($type == 'array') {
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $lead_allocation_time = $this->setting_model->where('key','lead_allocation_time')->get()['value'];
            $this->db->insert('leads', [
                'user_id' => $token_data->id,
                'lead_code' => rand(999, 9999),
                'created_at' => date('Y-m-d H:i:s'),
                'status' => 1,
                'validity' => date('Y-m-d H:i:s', strtotime("+".(count($_POST['vendors'])*$lead_allocation_time)." minutes"))
            ]);
            $lead_id = $this->db->insert_id();
            $reporting_end_time = '';$reporting_start_time= date('Y-m-d H:i:s');
            foreach ($_POST['vendors'] as $vendor) {
                if(empty($reporting_end_time)){
                    $reporting_end_time = date('Y-m-d H:i:s', strtotime($reporting_start_time. "+$lead_allocation_time minutes"));
                } else {
                    $reporting_start_time = $reporting_end_time;
                    $reporting_end_time = date('Y-m-d H:i:s', strtotime($reporting_end_time. "+$lead_allocation_time minutes"));
                }
                $data[] = array(
                    'lead_id' => $lead_id,
                    'vendor_id' => $vendor['vendor_id'],
                    'reporting_start_time' => $reporting_start_time,
                    'reporting_end_time' => $reporting_end_time,
                    'status' => 1
                );
            }
            $id = $this->db->insert_batch('lead_details', $data);
            $this->set_response_simple($id, 'We Will Contact You Soon', REST_Controller::HTTP_CREATED, TRUE);
        }
    }
    
    public function lead_cron_get(){
        $this->db->select('lead_details.id, lead_details.lead_id, leads.lead_code, leads.user_id, leads.validity, lead_details.vendor_id, lead_details.reporting_start_time, lead_details.reporting_end_time');
        $this->db->join('lead_details', 'leads.id = lead_details.lead_id');
        $this->db->where("leads.validity >=",  date('Y-m-d H:i:s'));
        $this->db->where("leads.created_at <=", date('Y-m-d H:i:s'));
        $this->db->where("leads.status", 1);
        $this->db->where("lead_details.reporting_start_time <=", date('Y-m-d H:i:s'));
        $this->db->where("lead_details.reporting_end_time >=", date('Y-m-d H:i:s'));
        $this->db->where("lead_details.status", 1);
        $query = $this->db->get('leads');
        $lead_list = $query->result_array();
        
        //$lead_list = $this->db->query("SELECT ld.id, ld.lead_id, l.lead_code, l.user_id, l.validity, ld.vendor_id, ld.reporting_start_time, ld.reporting_end_time FROM `leads` as l JOIN lead_details as ld ON l.id = ld.lead_id WHERE l.validity >= ".date('Y-m-d H:i:s')." AND l.created_at <= ".date('Y-m-d H:i:s')." AND l.status = 1 AND ld.reporting_start_time <= ".date('Y-m-d H:i:s')." AND ld.reporting_end_time >= ".date('Y-m-d H:i:s')." AND ld.status = 1")->result_array();
        if(! empty($lead_list)){
            foreach ($lead_list as $key => $lead){
                $is_exist = $this->db->where([ 'vendor_id' => $lead['vendor_id'], 'lead_id' => $lead['lead_id']])->get('vendor_leads')->result_array();
                if(! $is_exist){
                    $this->db->insert('vendor_leads', [
                        'vendor_id' => $lead['vendor_id'],
                        'lead_id' => $lead['lead_id'],
                        'lead_status' => 1
                    ]);
                    $this->send_notification($lead['vendor_id'], VENDOR_APP_CODE, "Alert", "New Lead is generated..!",['notification_type' => $this->notification_type_model->where(['app_details_id' => 2, 'notification_code' => 'LD'])->get()]);
                    $this->db->where([ 'vendor_id' => $lead['vendor_id'], 'lead_id' => $lead['lead_id']]);
                    $this->db->update('lead_details', ['status' => 2]);
                }
            }
        }
        $this->set_response_simple($lead_list, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }



      /**
     * User address
     *
     * To Manage address
     *
     * @author Trupti
     * @param string $type
     */
    public function test_sale_post($type = 'r')
    {
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        if ($type == 'c') {
            $this->form_validation->set_rules($this->state_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                

                $id = $this->state_model->insert([
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('name'),
                ]);
                $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }
        } elseif ($type == 'r') {
           
            $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }  elseif ($type == 'u') {
            $this->form_validation->set_rules($this->users_address_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $ll = $this->users_address_model->update([
                    'id' => $this->input->post('id'),
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('name')
                  
                ], 'id');
                $this->set_response_simple($ll, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
            }
        } elseif ($type == 'd') {
            $ll = $this->state_model->delete([
                'id' => $this->input->post('id')
            ]);
            $this->set_response_simple($ll, 'Deleted..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
}

?>